package com.kuliza.lending.common.utils;

public class NumberUtil {

  public static boolean isNumeric(String text) {

    try {
      Long.parseLong(text);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }
}

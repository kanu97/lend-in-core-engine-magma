package com.kuliza.lending.authorization.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.authorization.config.keycloak.KeyCloakService;
import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.pojo.UserDetails;
import com.kuliza.lending.pojo.UserLoginDetails;
import com.kuliza.lending.pojo.UserRoles;
import com.kuliza.lending.pojo.UserToken;

/****
 * 
 * @author Naveen Dhalaria
 *
 */
@RestController
@RequestMapping("/auth")
public class AuthorizationController {

	@Autowired
	KeyCloakManager keyCloakManager;

	@Autowired
	KeyCloakService keyCloakService;

	/**
	 * This method is to register a User
	 * 
	 * @return UserRepresentation is Successfully created.
	 */
	@PostMapping(value = "/signup")
	public ApiResponse signup(@Valid @RequestBody UserDetails user) {
		return keyCloakManager.signup(user.getFirstname(), user.getLastname(), user.getUsername(), user.getEmail(),
				user.getPassword(), user.getRoles(), user.getAttributes());
	}

	/**
	 * This method is used to login using email and password
	 * 
	 * @param user - details for user
	 * @return json response with user token and other details
	 */
	@PostMapping(value = "/login")
	public ApiResponse login(@Valid @RequestBody UserLoginDetails user) {
		return keyCloakManager.loginWithEmailAndPassword(user.getEmail(), user.getPassword());
	}

	/**
	 * This method will take refresh token and return access token and other user
	 * details if token is vaild
	 * 
	 * @param refresh_token
	 * @return json response with token details
	 */
	@PostMapping(value = "/refresh-token")
	public ApiResponse refresh(@RequestBody UserToken token) {
		return keyCloakManager.getAccessTokenByRefreshToken(token.getRefresh_token());
	}

	/**
	 * This method is used for logout user from current active session
	 * 
	 * @param token
	 * @param request
	 * @return Success or fail response
	 */
	@PostMapping(value = "/logout")
	public ApiResponse logout(@RequestBody UserToken token, HttpServletRequest request) {
		return keyCloakManager.logout(token.getRefresh_token());
	}

	/**
	 * to get all the registered users
	 * 
	 * @return usernames
	 */
	@GetMapping("/all-users")
	public ApiResponse allUsersList() {
		List<String> allUsers = keyCloakService.allUsers();
		return new ApiResponse(HttpStatus.OK, "OK", allUsers);
	}

	/***
	 * This end Point is for clear all the Active Sessions for a user associated
	 * with user while creating them
	 * 
	 * @param request
	 * @return
	 */
	@PostMapping("/sessions/logout")
	public ApiResponse allSessionLogout(HttpServletRequest request, @RequestBody Map<String, String> user) {

		UserRepresentation rep = keyCloakService.findUserByEmail(user.get("email"));
		String userID = null;
		if (rep != null) {
			userID = rep.getId();
		}
		keyCloakService.logoutUserFromAllSessions(userID);
		return new ApiResponse(HttpStatus.OK, "OK", "cleared all Sessions for User " + rep.getEmail());
	}

	/***
	 * This end Point is for creating client level roles these roles can be
	 * associated with user while creating them
	 * 
	 * @param UserRoles
	 * @return a Map for roles with status "Created" or "Existing" role
	 */
	@PostMapping("/roles/add-new-role")
	public ApiResponse createNewRole(@RequestBody UserRoles roles, HttpServletRequest request) {

		Map<String, String> rolesMap = keyCloakManager.createNewRole(roles.getRoles());
		return new ApiResponse(HttpStatus.OK, "OK", rolesMap);
	}

	/***
	 * This end Point is for adding client level roles to the user
	 * 
	 * @param UserRoles
	 * @return a Map for roles with status "Created" or "Existing" role
	 */
	@PostMapping("/users/roles/add-user-roles")
	public ApiResponse adduserToRole(@RequestBody UserRoles user) {

		return new ApiResponse(HttpStatus.OK, "OK", keyCloakManager.addUserToRole(user.getEmail(), user.getRoles()));

	}

	/***
	 * This end Point is for creating new Groups in Realm
	 * 
	 * @param requestBody
	 * @return a Map for roles with status "Created" or "Existing" Group
	 */
	@PostMapping("/groups/create-new-group")
	public ApiResponse createNewGroups(@RequestBody Map<String, List<String>> requestBody) {

		return new ApiResponse(HttpStatus.OK, "OK", keyCloakManager.createGroups(requestBody.get("groups")));
	}

	/***
	 * This end Point is for Listing all Groups in Realm
	 * 
	 * @param requestBody
	 * @return a Map for roles with status "Created" or "Existing" Group
	 */
	@GetMapping("/groups/all-groups")
	public ApiResponse createNewGroups() {

		return new ApiResponse(HttpStatus.OK, "OK", keyCloakService.AllGroups());
	}

}

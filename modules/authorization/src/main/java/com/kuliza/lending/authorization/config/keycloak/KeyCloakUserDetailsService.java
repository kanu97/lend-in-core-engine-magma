package com.kuliza.lending.authorization.config.keycloak;

import java.util.List;
import java.util.Map;

import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class KeyCloakUserDetailsService {

	@Autowired
	KeyCloakService keyCloakService;

	public UserDetails loadUserByKeyCloakUserId(String keyCloakUserId) {

		UserRepresentation userRepresentation = this.keyCloakService.findUserByKeyCloakUserId(keyCloakUserId);
		return this.toUserDetailsWithEmail(userRepresentation);
	}

	private UserDetails toUserDetailsWithEmail(UserRepresentation userRepresentation) {
		List<String> roles=this.keyCloakService.getClientRoleListForUser(userRepresentation.getId());
		String []rolesArr=new String[roles.size()];
		rolesArr=roles.toArray(rolesArr);
		return org.springframework.security.core.userdetails.User.withUsername(userRepresentation.getEmail())
				.password("").roles(rolesArr).build();
	}

	public String getUserIdFromRepresentation(String keyCloakUserId) {
		String userId="";
		UserRepresentation userRepresentation=this.keyCloakService.findUserByKeyCloakUserId(keyCloakUserId);
		Map<String, List<String>> attributes=userRepresentation.getAttributes();
		if(attributes!=null && attributes.containsKey("user_id"))
		  userId=attributes.get("user_id").get(0);
		return userId;

	}
}

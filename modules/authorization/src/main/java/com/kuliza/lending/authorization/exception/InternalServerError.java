package com.kuliza.lending.authorization.exception;

public class InternalServerError extends RuntimeException {

	private static final long serialVersionUID = -925106486781498808L;

	public InternalServerError(String message) {
		super(message);
	}
}

package com.kuliza.lending.journey.customer_dashboard.pojo;

public class CustomerDashboardDetails {
	private String borrowerFullName;
	private String selfieLink;
	private String date;
	private String remainingAmount;
	private String totalAmount;
	private String remainingTenure;
	private String emailId;
	private String contactNumber;
	private String customerSince;

	public CustomerDashboardDetails() {
		super();
		this.borrowerFullName = "";
		this.selfieLink = "";
		this.date = "";
		this.remainingAmount = "";
		this.totalAmount = "";
		this.remainingTenure = "";
		this.emailId = "";
		this.contactNumber = "";
		this.customerSince = "";
	}

	public CustomerDashboardDetails(String borrowerFullName, String selfieLink, String date, String remainingAmount,
			String totalAmount, String remainingTenure, String emailId, String contactNumber, String customerSince) {
		super();
		this.borrowerFullName = borrowerFullName;
		this.selfieLink = selfieLink;
		this.date = date;
		this.remainingAmount = remainingAmount;
		this.totalAmount = totalAmount;
		this.remainingTenure = remainingTenure;
		this.emailId = emailId;
		this.contactNumber = contactNumber;
		this.customerSince = customerSince;
	}

	public String getBorrowerFullName() {
		return borrowerFullName;
	}

	public void setBorrowerFullName(String borrowerFullName) {
		this.borrowerFullName = borrowerFullName;
	}

	public String getSelfieLink() {
		return selfieLink;
	}

	public void setSelfieLink(String selfieLink) {
		this.selfieLink = selfieLink;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(String remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getRemainingTenure() {
		return remainingTenure;
	}

	public void setRemainingTenure(String remainingTenure) {
		this.remainingTenure = remainingTenure;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getCustomerSince() {
		return customerSince;
	}

	public void setCustomerSince(String customerSince) {
		this.customerSince = customerSince;
	}

}

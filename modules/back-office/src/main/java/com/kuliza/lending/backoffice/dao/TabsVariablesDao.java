package com.kuliza.lending.backoffice.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.backoffice.models.Tabs;
import com.kuliza.lending.backoffice.models.TabsVariablesMapping;
import com.kuliza.lending.backoffice.models.Variables;

@Repository
public interface TabsVariablesDao extends CrudRepository<TabsVariablesMapping, Long> {

	public TabsVariablesMapping findByTabAndVariableAndIsDeleted(Tabs tab, Variables variable, boolean isDeleted);

}

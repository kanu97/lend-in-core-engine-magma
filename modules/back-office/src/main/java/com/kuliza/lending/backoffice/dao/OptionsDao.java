package com.kuliza.lending.backoffice.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.backoffice.models.Options;
import com.kuliza.lending.backoffice.models.Variables;

@Repository
public interface OptionsDao extends CrudRepository<Options, Long> {

	public Options findById(long id);

	public Options findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<Options> findByLabel(String label);

	public List<Options> findByLabelAndIsDeleted(String label, boolean isDeleted);

	public Options findByOptionKeyAndIsDeleted(String optionKey, boolean isDeleted);

	public Options findByOptionKeyAndVariableAndIsDeleted(String optionKey, Variables variable, boolean isDeleted);

}

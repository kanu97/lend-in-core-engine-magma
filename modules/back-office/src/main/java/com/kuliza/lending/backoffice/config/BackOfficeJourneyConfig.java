package com.kuliza.lending.backoffice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "back-office")
public class BackOfficeJourneyConfig {

	private String journeyName;

	public String getJourneyName() {
		return journeyName;
	}

	public void setJourneyName(String journeyName) {
		if (journeyName == null) {
			throw new IllegalArgumentException("Backoffice journey name not defined!");
		}
		this.journeyName = journeyName;
	}

}

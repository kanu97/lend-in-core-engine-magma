package com.kuliza.lending.backoffice.pojo.dashboard;

import java.util.Map;

import javax.validation.constraints.NotNull;

public class SaveApplicationData {

	@NotNull(message = "data cannot be null.")
	private Map<String, Object> data;

	public SaveApplicationData() {
		super();
	}

	public SaveApplicationData(Map<String, Object> data) {
		super();
		this.data = data;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

}

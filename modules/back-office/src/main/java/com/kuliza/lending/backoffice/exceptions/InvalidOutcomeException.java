package com.kuliza.lending.backoffice.exceptions;

public class InvalidOutcomeException extends RuntimeException {

	public InvalidOutcomeException() {
		super();
	}

	public InvalidOutcomeException(String message) {
		super(message);
	}
}

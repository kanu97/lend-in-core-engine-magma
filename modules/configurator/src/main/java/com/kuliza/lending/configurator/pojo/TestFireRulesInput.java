package com.kuliza.lending.configurator.pojo;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestFireRulesInput {

	@NotNull(message = "productIds is a required key")
	List<Long> productIds;

	@NotNull(message = "inputData is a required key")
	Map<String, Object> inputData;

	String userId;

	public TestFireRulesInput() {
		this.productIds = new ArrayList<>();
		this.inputData = new HashMap<>();
	}

	public TestFireRulesInput(List<Long> productIds, Map<String, Object> inputData) {
		this.productIds = productIds;
		this.inputData = inputData;
	}

	public List<Long> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<Long> productIds) {
		this.productIds = productIds;
	}

	public Map<String, Object> getInputData() {
		return inputData;
	}

	public void setInputData(Map<String, Object> inputData) {
		this.inputData = inputData;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("productIds : " + productIds + ", ");
	// inputData.append("inputData : " + inputData.toString());
	// inputData.append(" }");
	// return inputData.toString();
	//
	// }

}

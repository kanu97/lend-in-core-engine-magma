package com.kuliza.lending.configurator.pojo;

import com.kuliza.lending.configurator.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UpdateProduct {

	@NotNull(message = "productName is a required key")
	@Pattern(regexp = Constants.NAME_REGEX, message = Constants.INVALID_PRODUCT_NAME_MESSAGE)
	@Size(max = Constants.MAX_LENGTH_NAME_STRING, message = Constants.PRODUCT_NAME_TOO_BIG_MESSAGE)
	private String productName;

	private String userId;
	private String productId;

	public UpdateProduct() {
		this.productName = "";
	}

	public UpdateProduct(String productName) {
		this.productName = productName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	// @Override
	// public String toString() {
	// StringBuilder updateVariable = new StringBuilder();
	// updateVariable.append("{ ");
	// updateVariable.append("productName : " + productName);
	// updateVariable.append(" }");
	// return updateVariable.toString();
	//
	// }

}

package com.kuliza.lending.configurator.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ExecutionDao extends CrudRepository<Execution, Long> {
	public Execution findById(long id);

	public Execution findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<Execution> findByIdentifier(String identifier);

	public List<Execution> findByIdentifierAndIsDeleted(String identifier, boolean isDeleted);
}

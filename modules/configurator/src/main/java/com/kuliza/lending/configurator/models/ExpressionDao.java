package com.kuliza.lending.configurator.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ExpressionDao extends CrudRepository<Expression, Long> {
	public Expression findById(long id);

	public Expression findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<Expression> findByProductId(long productId);

	public List<Expression> findByProductIdAndIsDeleted(long productId, boolean isDeleted);

	public List<Expression> findByGroupId(long groupId);

	public List<Expression> findByGroupIdAndIsDeleted(long groupId, boolean isDeleted);

	public Expression findByIdAndGroupIdAndIsDeleted(long id, long groupId, boolean isDeleted);

	public Expression findByIdAndProductIdAndIsDeleted(long id, long productId, boolean isDeleted);
}

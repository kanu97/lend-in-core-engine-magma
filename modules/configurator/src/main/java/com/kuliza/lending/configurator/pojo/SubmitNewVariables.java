package com.kuliza.lending.configurator.pojo;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class SubmitNewVariables {

	@NotNull(message = "newVariablesList is a required key")
	@Size(min = 1, message = "Variable List cannot be empty")
	@Valid
	private List<NewVariable> newVariablesList;

	private String productId;
	private String userId;

	public SubmitNewVariables() {
		this.newVariablesList = new ArrayList<>();
	}

	public SubmitNewVariables(List<NewVariable> newVariablesList) {
		this.newVariablesList = newVariablesList;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public List<NewVariable> getNewVariablesList() {
		return newVariablesList;
	}

	public void setNewVariablesList(List<NewVariable> newVariablesList) {
		this.newVariablesList = newVariablesList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("newVariablesList : [ ");
	// for (int i = 0; i < newVariablesList.size(); i++) {
	// inputData.append(newVariablesList.get(i).toString());
	// if (i < newVariablesList.size() - 1) {
	// inputData.append(", ");
	// }
	// }
	// inputData.append("] }");
	// return inputData.toString();
	//
	// }

}